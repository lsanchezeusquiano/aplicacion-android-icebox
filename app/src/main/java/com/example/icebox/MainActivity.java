package com.example.icebox;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.squareup.picasso.Picasso;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase basedatabase;
    private DatabaseReference referencia;
    private Button btnNuevo;
    private ListView listViewContactos;
    private ArrayList<Productos> productos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        basedatabase = FirebaseDatabase.getInstance();
        referencia = basedatabase.getReferenceFromUrl(ReferenciasFirebase.URL_DATABASE + ReferenciasFirebase.DATABASE_NAME);
        btnNuevo = findViewById(R.id.btnNuevo);
        listViewContactos = findViewById(R.id.list);

        obtenerProductos();

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,Agregar.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void obtenerProductos() {
        productos = new ArrayList<>();
        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Productos producto = dataSnapshot.getValue(Productos.class);
                producto.setCodigo(dataSnapshot.getKey());
                productos.add(producto);
                actualizarLista();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        referencia.addChildEventListener(listener);
    }

    private void actualizarLista() {
        MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.joyas_card, productos);
        listViewContactos.setAdapter(adapter);
    }
    class MyArrayAdapter extends ArrayAdapter<Productos>
    { Context context; int textViewRecursoId;
        ArrayList<Productos> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Productos> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);
            CardView cardView = view.findViewById(R.id.imageViewProducto);
            ImageView productoIMG = cardView.findViewById(R.id.imgProducto);

            TextView lblNombre = view.findViewById(R.id.textViewNombre);
            TextView lblPrecio = view.findViewById(R.id.textViewPrecio);
            TextView lblCodigo = view.findViewById(R.id.textViewCodigo);
            ImageButton btnModificar = view.findViewById(R.id.btnActualizar);
            ImageButton btnBorrar = view.findViewById(R.id.btnEliminar);

            lblNombre.setText(objects.get(position).getNombre());
            lblPrecio.setText("Precio: "+objects.get(position).getPrecio());
            lblCodigo.setText("Codigo: "+objects.get(position).getCodigo());
            Picasso.get().load(objects.get(position).getUrl()).into(productoIMG);

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    borrarContacto(objects.get(position).getCodigo());
                    objects.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),"Producto eliminado con exito", Toast.LENGTH_SHORT).show();
                }
            });
            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, Agregar.class);


                    intent.putExtra("nombre", objects.get(position).getNombre());
                    intent.putExtra("codigo", objects.get(position).getCodigo());
                    intent.putExtra("precio", objects.get(position).getPrecio());
                    intent.putExtra("url", objects.get(position).getUrl());

                    context.startActivity(intent);
                    finish();
                }
            }); return
                    view;
        }
    }
    public void borrarContacto(String childIndex){
        referencia.child(String.valueOf(childIndex)).removeValue();
    }
}