package com.example.icebox;

import java.io.Serializable;

public class Productos implements Serializable {
    private String codigo,nombre,precio,url;

    public Productos() {
    }

    public Productos(String codigo, String nombre, String precio,String url) {
        this.codigo = codigo;
        this.url=url;
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
