package com.example.icebox;

import android.Manifest;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class Agregar extends AppCompatActivity {
    int REQUEST_CODE = 200;
    private Button btnGuardar;
    private Button btnRegreasr;
    private Button btnLimpiar;
    private TextView txtNombre;
    private TextView txtCodigo;
    private TextView txtPrecio;
    private ImageView img;
    private FirebaseDatabase basedatabase;
    private DatabaseReference referencia;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Productos producto;
    private String id;
    private String urlFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);
        basedatabase = FirebaseDatabase.getInstance();
        referencia = this.basedatabase.getReferenceFromUrl(ReferenciasFirebase.URL_DATABASE + ReferenciasFirebase.DATABASE_NAME);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegreasr = findViewById(R.id.btnRegresar);
        txtNombre = findViewById(R.id.txtNombre);
        txtCodigo = findViewById(R.id.txtCodigo);
        txtPrecio = findViewById(R.id.txtPrecio);
        img = findViewById(R.id.imgProducto);

        solicitarPermisos();
        Intent intent = getIntent();

        if (intent != null && intent.getExtras() != null) {
            String nombre = intent.getStringExtra("nombre");
            String codigo = intent.getStringExtra("codigo");
            String precio = intent.getStringExtra("precio");
            urlFoto=intent.getStringExtra("url");

            txtCodigo.setEnabled(false);
            txtNombre.setText(nombre);
            txtCodigo.setText(codigo);
            txtPrecio.setText(precio);
            Picasso.get().load(urlFoto).into(img);
        }


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigo = txtCodigo.getText().toString();
                String nombre = txtNombre.getText().toString();
                String precio = txtPrecio.getText().toString();
                String imagen = urlFoto;

                if (codigo.isEmpty() || nombre.isEmpty() || precio.isEmpty() || urlFoto == null) {
                    Toast.makeText(Agregar.this, "Todos los datos son requeridos", Toast.LENGTH_SHORT).show();
                } else {
                    if (verificarConexion()){
                        insertarProducto(codigo,nombre, precio,imagen);
                        limpiarCampos();
                        Toast.makeText(Agregar.this, "Producto Subido Exitosamente", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(Agregar.this, "No hay conexión a Internet para subir el producto", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegreasr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Agregar.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/");
                startActivityForResult(intent.createChooser(intent, "Seleccione una apicacion"), 10);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri path = data.getData();
            img.setImageURI(path);
            urlFoto = path.toString();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void solicitarPermisos() {
        int permisos = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES);

        if (permisos == PackageManager.PERMISSION_GRANTED) {
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_MEDIA_IMAGES}, REQUEST_CODE);
        }
    }

    private boolean verificarConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    private void limpiarCampos() {
        txtNombre.setText("");
        txtCodigo.setText("");
        txtPrecio.setText("");
        img.setImageResource(R.drawable.img);
    }

    private void insertarProducto(String codigo, String nombre, String precio, String urlFoto) {
        String urlFotoActual = getIntent().getStringExtra("url");
        if (urlFoto != null && !urlFoto.equals(urlFotoActual)) {
            StorageReference imagenRef = storageReference.child(codigo + ".jpg");
            try {
                InputStream imageStream = getContentResolver().openInputStream(Uri.parse(urlFoto));
                UploadTask uploadTask = imagenRef.putStream(imageStream);
                uploadTask.addOnSuccessListener(taskSnapshot -> {
                    imagenRef.getDownloadUrl().addOnSuccessListener(uri -> {
                        Productos producto = new Productos(codigo, nombre, precio, uri.toString());

                        referencia.child(codigo).setValue(producto)
                                .addOnSuccessListener(aVoid -> {
                                    Toast.makeText(Agregar.this, "Producto Subido Exitosamente", Toast.LENGTH_SHORT).show();
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(Agregar.this, "Error al subir el producto", Toast.LENGTH_SHORT).show();
                                });
                    });
                }).addOnFailureListener(e -> {
                    Toast.makeText(Agregar.this, "Error al subir la imagen", Toast.LENGTH_SHORT).show();
                });

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(Agregar.this, "Error al abrir la imagen", Toast.LENGTH_SHORT).show();
            }
        } else {
            Productos producto = new Productos(codigo, nombre, precio, urlFoto);

            referencia.child(codigo).setValue(producto)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(Agregar.this, "Producto Actualizado Exitosamente", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(Agregar.this, "Error al actualizar el producto", Toast.LENGTH_SHORT).show();
                    });
        }
    }
}